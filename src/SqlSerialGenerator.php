<?php

namespace Drupal\serials;

/**
 * Generate a serial using the database.
 */
class SqlSerialGenerator implements SerialGeneratorInterface {

  /**
   * The serials generator.
   *
   * @var \Drupal\serials\SerialsGeneratorInterface
   */
  protected $serialsGenerator;

  /**
   * The key.
   *
   * @var string
   */
  protected $key;

  /**
   * SqlSerialGenerator constructor.
   *
   * @param \Drupal\serials\SerialsGeneratorInterface $serialsGenerator
   *   The serials generator.
   * @param string $key
   *   The key.
   */
  public function __construct(SerialsGeneratorInterface $serialsGenerator, string $key) {
    $this->serialsGenerator = $serialsGenerator;
    $this->key = $key;
  }

  /**
   * {@inheritDoc}
   */
  public function generate() {
    return $this->serialsGenerator->generate($this->key);
  }

  /**
   * {@inheritDoc}
   */
  public function reset(int $value, bool $force = FALSE) {
    $this->serialsGenerator->reset($this->key, $value, $force);
  }

}
