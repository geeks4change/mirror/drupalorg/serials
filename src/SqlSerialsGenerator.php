<?php

namespace Drupal\serials;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Generate multiple serials using the database.
 */
class SqlSerialsGenerator implements SerialsGeneratorInterface {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The table name.
   *
   * @var string
   */
  private $table;

  /**
   * The key column name.
   *
   * @var string
   */
  private $valueColumn;

  /**
   * The value column name.
   *
   * @var string
   */
  private $keyColumn;

  /**
   * SerialsService constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The DB connection.
   * @param string $table
   *   The table name.
   * @param string $keyColumn
   *   The key column name.
   * @param string $valueColumn
   *   The value column name.
   */
  public function __construct(Connection $connection, string $table, string $keyColumn, string $valueColumn) {
    $this->connection = $connection;
    $this->table = $table;
    $this->keyColumn = $keyColumn;
    $this->valueColumn = $valueColumn;
  }

  /**
   * Increment keys.
   *
   * @param $keys
   *   The keys.
   *
   * @return int[]
   *   The serials.
   */
  protected function increment($keys) {
    $transaction = $this->connection->startTransaction();
    // @fixme Explain why this is safe in all relevant isolation levels.
    $this->connection->update($this->table)
      ->expression($this->valueColumn, $this->valueColumn . ' + 1')
      ->where($this->keyColumn . ' IN (:keys[])', [':keys[]' => $keys])
      ->execute();
    $serials = $this->connection->select($this->table)
      ->fields($this->table, [$this->keyColumn, $this->valueColumn])
      ->execute()
      ->fetchAllKeyed();
    return $serials;
  }

  /**
   * {@inheritDoc}
   */
  public function generateMultiple(array $keys): array {
    try {
      // Let's be optimistic that all keys are already in and save us a query.
      return $this->increment($keys);
    } catch (DatabaseExceptionWrapper $e) {
      // Exceptionally, first add all keys. If the exception was something
      // different from  missing keys, this will now show up.
      $this->resetMultiple(array_fill_keys($keys, 0));
      return $this->increment($keys);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function generate(string $key): int {
    $serials = $this->generateMultiple([$key]);
    return reset($serials);
  }

  /**
   * {@inheritDoc}
   */
  public function resetMultiple(array $keyedValues, bool $force = FALSE) {
    foreach ($keyedValues as $key => $value) {
      if ($force) {
        // @see https://www.drupal.org/docs/8/api/database-api/merge-queries#s-just-set-it
        $this->connection->merge($this->table)
          ->key($this->keyColumn, $key)
          ->fields([$this->valueColumn => $value])
          ->execute();
      }
      else {
        // @see https://www.drupal.org/docs/8/api/database-api/merge-queries#s-conditional-set
        $this->connection->merge($this->table)
          ->key($this->keyColumn, $key)
          ->fields([$this->valueColumn => $value])
          ->expression($this->valueColumn, "max({$this->valueColumn}, :value)", [':value' => $value])
          ->execute();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function reset(string $key, int $value, bool $force = FALSE) {
    $this->resetMultiple([$key => $value]);
  }

}
